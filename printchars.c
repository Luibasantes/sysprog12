#include "csapp.h"

typedef struct
{
//	int *buf; /* Buffer array */
	char *buf;
	int buf_size;

	int n; /* Maximum number of slots */
	int front; /* buf[(front+1)%n] is first item */
	int rear; /* buf[rear%n] is last item */
	sem_t mutex; /* Protects accesses to buf */
	sem_t slots; /* Counts available slots */
	sem_t items; /* Counts available items */
} sbuf_t;



sbuf_t buffer;
int volatile value = 1;



void sbuf_init(sbuf_t *sp, int n)
{
	sp->buf = Calloc(n, sizeof(int));
	sp->n = n; /* Buffer holds max of n items */
	sp->front = sp->rear = 0; /* Empty buffer iff front == rear */
	Sem_init(&sp->mutex, 0, 1); /* Binary semaphore for locking */
	Sem_init(&sp->slots, 0, n); /* Initially, buf has n empty slots */
	Sem_init(&sp->items, 0, 0); /* Initially, buf has zero data items */

	sp->buf_size=0;
}

void sbuf_deinit(sbuf_t *sp)
{
	Free(sp->buf);
}

void sbuf_insert(sbuf_t *sp, int item)
{
	P(&sp->slots); /* Wait for available slot */
	P(&sp->mutex); /* Lock the buffer */
	sp->buf[(++sp->rear)%(sp->n)] = item; /* Insert the item */

	sp->buf_size++;

	V(&sp->mutex); /* Unlock the buffer */
	V(&sp->items); /* Announce available item */
}

char sbuf_remove(sbuf_t *sp)
{
	int item;
	P(&sp->items); /* Wait for available item */
	P(&sp->mutex); /* Lock the buffer */
	item = sp->buf[(++sp->front)%(sp->n)]; /* Remove the item */

	sp->buf_size--;

	V(&sp->mutex); /* Unlock the buffer */
	V(&sp->slots); /* Announce available slot */
	return item;
}

//

void *file_to_buffer(void *argv)
{
	char *filename = (char *)argv;
	int fd;
	char c;

	printf("Abriendo archivo %s...\n",filename);
	fd = Open(filename, O_RDONLY, 0);

	while(Read(fd, &c, 1))
	{
		sbuf_insert(&buffer, c);
		usleep(50);
	}
	Close(fd);

	value=0;

	return NULL;
}


void *buffer_to_output()
{
	while(buffer.buf_size > 0 || value == 1)
	{
		printf("%c", sbuf_remove(&buffer));
		usleep(1000);
	}

	printf("\n");

	return NULL;
}



//

int main(int argc, char **argv)
{
	char *filename;
//	int fd;
//	char c;
	struct stat fileStat;

	pthread_t tid1, tid2;

	if (argc != 2)
	{
		printf("usage: %s <filename>\n", argv[0]);
		exit(0);
	}
	filename = argv[1];


	if (stat(filename, &fileStat)<0)
	{
		printf("Archivo %s no fue encontrado\n",filename);
	}
	else
	{

		sbuf_init(&buffer, 10);

		Pthread_create(&tid1, NULL, file_to_buffer, filename);
		Pthread_create(&tid2, NULL, buffer_to_output, NULL);

		Pthread_join(tid1, NULL);
		Pthread_join(tid2, NULL);

	}

	return 0;
}



